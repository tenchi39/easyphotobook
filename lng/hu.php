<?php

$lng = array(
	"home" => "Kezdőlap", 
	"directories" => "Könyvtárak", 
	"subdirectories" => "Alkönyvtárak", 
	"count_subdirectories" => "Könyvtár", 
	"download_original" => "Letöltés eredeti felbontásban", 
	"photos" => "Fénykép", 
	"photos_in" => "Photos in", 
	"go_back" => "Vissza", 
	"copy_gallery_url" => "URL másolása", 
	"gallery_url_copied_to_clipboard" => "A galéria URL-je a vágólapra lett másolva!", 
	"permission_warning" => "Hiba! A következő könyvtárnak írhatónak kell lennie: /tmp", 
	"readme" => "Readme", 
	"switch_to_dark_mode" => "Sötét mód", 
	"switch_to_light_mode" => "Világos mód", 
	"lightbox_images" => "Képek: %1/%2", 
	"enter_password_to_unlock" => "Add meg a jelszót a feloldáshoz", 
	"submit" => "Tovább"
);