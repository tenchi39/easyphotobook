<?php

$lng = array(
	"home" => "Home", 
	"directories" => "Directories", 
	"subdirectories" => "Subdirectories", 
	"count_subdirectories" => "Directories", 
	"download_original" => "Download original", 
	"photos" => "Photos", 
	"photos_in" => "Photos in", 
	"go_back" => "Go back", 
	"copy_gallery_url" => "Copy URL", 
	"gallery_url_copied_to_clipboard" => "Gallery URL copied to clipboard!", 
	"permission_warning" => "Warning! The following directory needs to be writable: /tmp", 
	"readme" => "Readme", 
	"switch_to_dark_mode" => "Dark mode", 
	"switch_to_light_mode" => "Light mode", 
	"lightbox_images" => "Image %1 of %2", 
	"enter_password_to_unlock" => "Enter password to unlock", 
	"submit" => "Submit"
);