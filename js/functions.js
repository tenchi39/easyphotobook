function share(url, success_message) {
	navigator.clipboard.writeText(url).then(function() {
	  alert(success_message);
	});
}

function createCookie(name,value,days) {
	if(days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}