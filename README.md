# EasyPhotobook

EasyPhotobook is a self-hosted gallery for your photos. All you need is PHP, no database required. Just upload your photos in any folder structure under the /data folder and EasyPhotobook will display your collection on the web.

## Features

- No database required
- Supports nested folders
- Supports hiding folders and files
- Supports password protection for folders
- Supports specifying cover image for folders
- Supports ascending and descending folder listing
- Supports adding image title with csv files
- Supports adding html text to top, bottom, left and right side of the layout
- Built-in slideshow
- Responsive, mobile friendly interface
- Grid or fluid layout for images
- Override default layout for specific folders
- Dark and light mode
- Configurable thumbnail and fullscreen image size
- Configurable column configuration
- Copy gallery URL to clipboard with one click

## How does it work?

When opening the website it will look through the contents of /data and will display the subfolders and images there. To make things snappy, it will create resized copies of your images in /tmp in thumbnail and fullscreen versions, so you can upload ultra HD images too without impacting performance if you have the available storage.

Be warned that because of the thumbnail generation, if you add new images, the first run will take quite a while and might possibly time out depending on your server's settings. No worries, just reload the page. Also be aware that if you open a subfolder, thumbnails will be generated for the contained images there also.

## Requirements

- PHP with GD
- mod_rewrite

## Installation

1. Clone or download the repository
2. Rename config-sample to config.php and set it up to your liking. Be sure to set the $baseURL variable correctly and to change the content of the $clearCacheKey variable.
3. Make sure that /tmp is writable
4. Upload your images in /data. You can use any number of sub and nested folders
5. Open the site in your browser and be patient during thumbnail generation

## Hiding folders

You can hide folders from appearing by adding a dot to the begining of the folder name. The folder can still be accessed using the direct URL which you can generate by Base64 encoding the folder path after the domain name. For example if your folder is "Test folder" in the /data folder, you need to Base64 encode the "data/Test folder" string and add it after your domain.

Example: https://myphotobook.com/ZGF0YS9UZXN0IGZvbGRlcg==

You can use a free online encoder to convert your path like https://www.base64encode.org/

## Password protection

You can protect your folders by adding a password.txt file in them. The content of this file will be the password that the users have to enter on the frontend to see the images.

## Adding a cover image for folders

If you add a cover.jpg file in a folder, itt will be used as the cover image for the folder and will not be shown once viewing the folder. With this you can specify any image as the cover image regardless if it was alphabetically the first or not.

## Override default layout

The default layout (grid or fluid) for images in a folder can be overriden by placing a layout.txt in the folder with the name of the desired layout as the content for the file.

## Adding html folder descriptions

You can easy modify the folder display layout by adding html text to the top, bottom, left or right side by adding a text_[direction].html file in the folder, for example "text_left.html".

## Image titles

You can add titles to your images in a folder by adding a titles.csv file in the folder. The first column is the filename while the second column will be used as the image title. Any other columns will not be processed.

## Hiding images

If you temporarily want to hide an image, add a dot to the begining of the filename. Such files will not be visible in the gallery.

## Deleting the cache

If you modify an already uploaded image, if the filename didn't change, EasyPhotobook will not regenerate the thumbnail for the image. You can either search for and delete the thumbnail and fullscreen copies in /tmp or delete all cached images with calling the URL specified in your config file. Be warned that this deletes all thumbnails, so all of them will be regenerated when you open the site in the browser.

Similarly, if you delete an image, it's thumbnail and fullscreen copy will not be deleted automatically from /tmp.

## Customizing

Feel free to modify the code to your liking.

- Appearance: /css/style-light.css and style-dark.css can be used as a starting point
- Languages: Make a copy of /lng/en.php to create a version for your language

## Licence

EasyPhotobook is licensed under The MIT License.

- 100% Free. EasyPhotobook is free to use in both commercial and non-commercial work.
- Attribution is required. This means you must leave my name, my homepage link, and the license info intact. None of these items have to be user-facing and can remain within the code.