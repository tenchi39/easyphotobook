<?php

function stripDirectory($filename) {
	$filename = explode('/', $filename);
	$filename = $filename[count($filename)-1];

	return $filename;
}

function getDirList($directory) {
	$dirlist = array();
	$dir_handle = @opendir($directory);
	while($file = @readdir($dir_handle)) {
		if($file != '.' and $file != '..') {
			if(is_dir($directory.'/'.$file)) {
				// Hide directories starting with a dot
				if(substr($file, 0, 1) != '.') {
					$dirlist[] = $directory.'/'.$file;
				}
			}
		}
	}
	@closedir($dir_handle);
	return $dirlist;
}

function getImageList($directory) {
	$filelist = array();
	$dir_handle = @opendir($directory);
	while($file = @readdir($dir_handle)) {
		if($file != '.' and $file != '..') {
			if(!is_dir($directory.'/'.$file)) {
				if(isImage($directory.'/'.$file)) {
					// Hide images starting with a dot
					if(substr($file, 0, 1) != '.' and $file != 'cover.jpg') {
						$filelist[] = $directory.'/'.$file;
					}
				}
			}
		}
	}
	@closedir($dir_handle);
	return $filelist;
}

function lng($id) {
	global $lng;
	return $lng[$id];
}

function getFilenames($directory, $prefix) {
	$files = array();
	$dir_handle = opendir($directory);
	while($file = readdir($dir_handle)) {
		if($file != '.' and $file != '..') {
			if($prefix != '') {
				$pos = strpos($file, $prefix);
				if($pos === false) {
				} elseif($pos == 0)	{
					$files[] = $directory.'/'.$file;
				}
			} else {
				$files[] = $directory.'/'.$file;
			}
		}
	}
	closedir($dir_handle);
	return $files;
}

function purgeTmp() {
	unset($files);
	$files = getFilenames('tmp', '');
	if(!empty($files)) {
		foreach($files as $value) {
			if($value != 'tmp/.gitkeep') {
				if(unlink($value)) {
					echo 'Deleting '.$value.'<br />';
				} else {
					echo 'Could not delete '.$value.' Please check file permissions!<br />';
				}
			}
		}
	}
}

function isImage($image) {
	$size = getimagesize($image);
	if(is_numeric($size[0]) and is_numeric($size[1])) {
		return true;
	} else {
		return false;
	}
}

function createImage($image, $maxwidth, $maxheight, $cut = false) {
	if($maxwidth == NULL or $maxheight == NULL) {
		$size = getimagesize($image);
		$width = $size[0];
		$height = $size[1];
	} else {
		$dimensions = imageSizeCalculator($image, $maxwidth, $maxheight, $cut);
		$width = $dimensions['width'];
		$height = $dimensions['height'];
	}

	$filename = explode('/', $image);
	if($filename[count($filename)-2] != '') {
		$prefix = $filename[count($filename)-2].'-';
	}
	$filename = $filename[count($filename)-1];
	$extension = explode('.', $filename);
	$extension = $extension[count($extension)-1];
	$filename = substr($filename, 0, (strlen($filename) - strlen($extension) - 1));
	$filename = 'tmp/image-'.$prefix.$width.'-'.$height.'-'.base64_encode($filename).'.'.$extension;
	if(!is_file($filename)) {
		if($cut == true) {
			$src_x = 0;
			$src_y = 0;
			if($width > $maxwidth) {
				$src_x = ($width - $maxwidth) / 2;
			}
			if($height > $maxheight) {
				$src_y = ($height - $maxheight) / 2;
			}
		}
		$mime_type = getMimetype($image);
		if($mime_type == 'image/gif') {
			$src_img = imagecreatefromgif($image);
			$dst_img = imagecreate($width, $height);
			imagecolortransparent($dst_img, imagecolorallocate($dst_img, 0, 0, 0));
			imagealphablending($dst_img, false);
			imagesavealpha($dst_img, true);
			imagecopyresampled($dst_img,$src_img,0,0,0,0,$width,$height,imagesx($src_img),imagesy($src_img));
			if($cut == true) {
				$cut_img = imagecreate($maxwidth, $maxheight);
				imagecolortransparent($cut_img, imagecolorallocate($cut_img, 0, 0, 0));
				imagealphablending($cut_img, false);
				imagesavealpha($cut_img, true);
				imagecopyresampled($cut_img,$dst_img,0,0,$src_x,$src_y,$maxwidth,$maxheight,$maxwidth,$maxheight);
				$dst_img = $cut_img;
			}
			imagegif($dst_img, $filename);
			imagedestroy($dst_img);
		}
		if($mime_type == 'image/jpeg' or $mime_type == 'image/pjpeg') {
			$src_img = imagecreatefromjpeg($image);
			$dst_img = imagecreatetruecolor($width, $height);
			imagecopyresampled($dst_img,$src_img,0,0,0,0,$width,$height,imagesx($src_img),imagesy($src_img));
			if($cut == true) {
				$cut_img = imagecreatetruecolor($maxwidth, $maxheight);
				imagecopyresampled($cut_img,$dst_img,0,0,$src_x,$src_y,$maxwidth,$maxheight,$maxwidth,$maxheight);
				$dst_img = $cut_img;
			}
			imagejpeg($dst_img, $filename, 95);
			imagedestroy($dst_img);
		}
		if($mime_type == 'image/png') {
			$src_img = imagecreatefrompng($image);
			$dst_img = imagecreatetruecolor($width, $height);
			imagecolortransparent($dst_img, imagecolorallocate($dst_img, 0, 0, 0));
			imagealphablending($dst_img, false);
			imagesavealpha($dst_img, true);
			imagecopyresampled($dst_img,$src_img,0,0,0,0,$width,$height,imagesx($src_img),imagesy($src_img));
			if($cut == true) {
				$cut_img = imagecreatetruecolor($maxwidth, $maxheight);
				imagecolortransparent($cut_img, imagecolorallocate($cut_img, 0, 0, 0));
				imagealphablending($cut_img, false);
				imagesavealpha($cut_img, true);
				imagecopyresampled($cut_img,$dst_img,0,0,$src_x,$src_y,$maxwidth,$maxheight,$maxwidth,$maxheight);
				$dst_img = $cut_img;
			}
			imagepng($dst_img, $filename);
			imagedestroy($dst_img);
		}
	}
	return $filename;
}

function imageSizeCalculator($image, $maxwidth, $maxheight, $cut = false) {
	if($image == '') {
		echo '<p>imageSizeCalculator() > Parameter missing: $image</p>';
		return;
	}
	if(!file_exists($image)) {
		echo '<p>imageSizeCalculator() > File missing: '.$image.'</p>';
		return;
	}
	$size = getimagesize($image);
	if($size[0] > $maxwidth or $size[1] > $maxheight) {
		if($size[0] > $size[1]) {
			// width is larger
			$width = $maxwidth;
			$percent = ($width / $size[0]);
			$height = ($size[1] * $percent);
			// if height is still too large
			if($height > $maxheight) {
				$height2 = $maxheight;
				$percent = ($height2 / $height);
				$width2 = ($width * $percent);
				$width = $width2;
				$height = $height2;
			}
		} else {
			// height is larger
			$height = $maxheight;
			$percent = ($height / $size[1]);
			$width = ($size[0] * $percent);
			// width is still too large
			if($width > $maxwidth) {
				$width2 = $maxwidth;
				$percent = ($width2 / $width);
				$height2 = ($height * $percent);
				$width = $width2;
				$height = $height2;
			}
		}
		if($cut == true) {
			if($width < $maxwidth) {
				// only width is smaller
				$percent = $maxwidth / $width;
				$width += ($maxwidth - $width);
				$height = $height * $percent;
			}
			if($height < $maxheight) {
				// only height is smaller
				$percent = $maxheight / $height;
				$height += ($maxheight - $height);
				$width = $width * $percent;
			}
		}
		$dimensions['width'] = round($width);
		$dimensions['height'] = round($height);
	} else {
		if($cut == true) {
			if($size[0] < $maxwidth and $size[1] < $maxheight) {
				if($maxwidth > $maxheight) {
					$size[0] = $maxwidth;
					$size[1] = $maxwidth;
				} elseif($maxheight > $maxwidth) {
					$size[0] = $maxheight;
					$size[1] = $maxheight;
				} else {
					$size[0] = $maxwidth;
					$size[1] = $maxheight;
				}
			}
		}
		$dimensions['width'] = $size[0];
		$dimensions['height'] = $size[1];
	}
	return $dimensions;
}

function getMimetype($file) {
	if($file == '') {
		echo '<p>getMimetype() > Parameter missing: $file</p>';
		return;
	}
	if(!file_exists($file)) {
		echo '<p>getMimetype() > File missing: '.$file.'</p>';
		return;
	}
	if(function_exists('finfo_open')) {
		$handle = finfo_open(FILEINFO_MIME);
		$mime_type = finfo_file($handle, $file);
		if(inIString('jpg', $mime_type) or inIString('jpeg', $mime_type)) {
			$mime_type = 'image/jpeg';
		}
		if(inIString('gif', $mime_type)) {
			$mime_type = 'image/gif';
		}
		if(inIString('png', $mime_type)) {
			$mime_type = 'image/png';
		}
	} else {
		$size = getimagesize($file);
		$mime_type = $size['mime'];
	}
	return $mime_type;
}

function inIString($needle, $haystack) {
	$pos = stripos($haystack, $needle);
	if($pos !== false) {
		return true;
	} else {
		return false;
	}
}