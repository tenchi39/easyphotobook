<?php

/* Basic settings */
$baseUrl = 'mydomain.com'; // Enter the site URL here
$siteName = 'EasyPhotobook'; // Site title in the header
$siteDescription = 'EasyPhotobook is a self-hosted gallery for your photos.'; // Site description
$siteLanguage = 'en'; // Site language, the selected language has to be present in /lng
$disableRightClick = false;
$defaultStyle = 'light'; // light and dark available
$defaultLayout = 'grid'; // grid or fluid available
$googleAnalyticsId = ''; // GA4 measurement ID (optional)

/* Cache key */
$clearCacheKey = '2a46e5e4038c2b93365f2956ce3c914b';
/* Use this key as the parameter when calling this URL to clear the cache:
https://mydomain.com/clear/2a46e5e4038c2b93365f2956ce3c914b
Of course change mydomain.com to your site's URL.
BE SURE TO CHANGE THIS TO SOME OTHER RANDOM STRING! */

/* Thumbnail size */
$thumbnailMaxWidth = 800;
$thumbnailMaxHeight = 600;

/* Web image size */
$largeMaxWidth = 1920;
$largeMaxHeight = 1080;

/* Grid layout */
$directoryColumns = 'small-6 medium-4 large-3';
$imageColumns = 'small-4 medium-4 large-3'; // Only in grid layout

/* Directory sorting order */
$sortingOrderDescending = false; // Leave on true for ascending