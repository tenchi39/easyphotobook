<?php

// Basic configuration
include('config.php');
// Functions
include('lib/functions.php');
// Language
include('lng/'.$siteLanguage.'.php');

// Theme
if($_COOKIE['style'] == '') {
	$_COOKIE['style'] = $defaultStyle;
}

// Clear image cache
if(substr($_REQUEST['url'], 0, 5) == 'clear') {
	$cacheKey = explode('/', $_REQUEST['url']);
	if($cacheKey[1] == $clearCacheKey) {
		echo '<a href="'.$baseUrl.'">Go back</a>';
		echo '<br />';
		echo '<br />';
		purgeTmp();
		echo '<br />';
		echo '<br />';
		echo '<a href="'.$baseUrl.'">Go back</a>';
		exit();
	}
}

// Check /tmp permissions
if(!is_writable('tmp')) {
	$permissionWarning .= '<div id="warning">'.lng('permission_warning').'</div>';
}

// Breadcrumb
$breadcrumbText.= '<nav aria-label="You are here:" role="navigation">';
$breadcrumbText.= '<ul class="breadcrumbs">';
$location = base64_decode($_REQUEST['url']);
if(!is_dir($location)) {
	$location = 'data';
}
$breadcrumbText.= '<li><i class="fi-home"></i>&nbsp;&nbsp;<a href="'.$baseUrl.'">'.strtoupper(lng('home')).'</a></li>';
$backLink = $baseUrl;
$shareName = lng('home').' | '.$siteName;
$breadcrumb = explode('/', $location);
$breadcrumb_link = 'data';
$i = 1;
foreach($breadcrumb as $crumb) {
	if($crumb != 'data') {
		$i++;
		$breadcrumb_link .= '/'.$crumb;
		if($i < count($breadcrumb)) {
			$backLink = base64_encode($breadcrumb_link);
			$breadcrumbText.= '<li><a href="'.base64_encode($breadcrumb_link).'">'.strtoupper($crumb).'</a></li>';
		} else {
			$location_text = $crumb;
			$shareName = $crumb.' | '.$siteName;
			$breadcrumbText.= '<li><span class="show-for-sr">Current: </span>'.strtoupper($crumb).'</span></li>';

		}
	}
}
$breadcrumbText.= '</ul>';
$breadcrumbText.= '</nav>';
// Texts
if(is_file($location.'/text_top.html')) {
	$text_top = file_get_contents($location.'/text_top.html');
}
if(is_file($location.'/text_bottom.html')) {
	$text_bottom = file_get_contents($location.'/text_bottom.html');
}
if(is_file($location.'/text_left.html')) {
	$text_left = file_get_contents($location.'/text_left.html');
}
if(is_file($location.'/text_right.html')) {
	$text_right = file_get_contents($location.'/text_right.html');
}


// Directories
$directories = getDirList($location);
if($sortingOrderDescending == true) {
	rsort($directories);
} else {
	asort($directories);
}
reset($directories);
$directoriesText = '';
if(!empty($directories)) {
	$directoriesText .= '<h4><i class="fi-folder"></i>&nbsp;&nbsp;';
	if($location == 'data') {
		$directoriesText .= lng('directories');
	} else {
		$directoriesText .= lng('subdirectories');
	}
	$directoriesText .= '</h4>';
	$directoriesText .= '<br />';
	$directoriesText .= '<div class="grid-x grid-margin-x">';
	foreach($directories as $dir) {
		// Subdirectories
		$subDirs = getDirList($dir);
		$directoriesText .= '<div class="directory-cell cell '.$directoryColumns.' text-left gallery-cell">';
		// List files to know how many images in directory and to show the first thumbnail
		$files = getImageList($dir);
		$count = count($files);
		// Cover
		asort($files);
		$files = array_values($files);
		if(is_file($dir.'/cover.jpg')) {
			$cover = $dir.'/cover.jpg';
		} else {
			$cover = $files[0];
		}
		if(is_file($dir.'/password.txt')) {
			$directoriesText .= '<a href="'.$baseUrl.base64_encode($dir).'"><img src="'.createImage('img/noimage-lock.png', $thumbnailMaxWidth, $thumbnailMaxHeight, true).'" /></a>';
		} elseif($cover != '') {
			$directoriesText .= '<a href="'.$baseUrl.base64_encode($dir).'"><img src="'.createImage($cover, $thumbnailMaxWidth, $thumbnailMaxHeight, true).'" /></a>';
		} else {
			$directoriesText .= '<a href="'.$baseUrl.base64_encode($dir).'"><img src="'.createImage('img/noimage.png', $thumbnailMaxWidth, $thumbnailMaxHeight, true).'" /></a>';
		}
		$directoriesText .= '<div class="gallery-cell-info" onclick="window.location=\''.$baseUrl.base64_encode($dir).'\';">';
		$directoriesText .= stripDirectory($dir);
		$directoriesText .= '<br />';
		$directoriesText .= $count.' '.lng('photos');
		$directoriesText .= '<br />';
		$directoriesText .= count($subDirs).' '.lng('count_subdirectories');
		$directoriesText .= '</div>';
		$directoriesText .= '</div>';
	}
	$directoriesText .= '</div>';
	$directoriesText .= '<br />';
}
// Images
$titleText = '';
$imagesText = '';
// Default share image
$shareImage = 'img/share/share.png';
$files = getImageList($location);
//isImage
if(!empty($files)) {
	// Delete non image files from the array
	$i = 0;
	foreach($files as $file) {
		if(!isImage($file)) {
			unset($files[$i]);
		}
		$i++;
	}
	// If the array is not empty
	if(!empty($files)) {
		// Check for titles
		unset($csv);
		unset($csv_titles);
		if(is_file($location.'/titles.csv')) {
			$csv = array_map('str_getcsv', file($location.'/titles.csv'));
			foreach($csv as $csv_line) {
				$csv_titles[$csv_line[0]] = $csv_line[1];
			}
		}
		// Layout
		if(is_file($location.'/layout.txt')) {
			$layout = trim(file_get_contents($location.'/layout.txt'));
		} else {
			$layout = $defaultLayout;
		}
		// Password
		if(is_file($location.'/password.txt')) {
			$password = trim(file_get_contents($location.'/password.txt'));
		}
		$titleText .= '<a class="float-right" href="'.$backLink.'">'.lng('go_back').' &raquo;</a>';
		$titleText .= '<h4><i class="fi-page-multiple"></i>&nbsp;&nbsp;'.$location_text.'</h4>';
		$titleText .= '<br />';
		// Top text
		if(!isset($password) or (isset($password) and $password == $_REQUEST['password'])) {
			if(!empty($text_top)) {
				$imagesText .= $text_top;
				$imagesText .= '<br />';
			}
		}
		if($directoriesText != '') {
			$imagesText .= '<h4><i class="fi-camera"></i>&nbsp;&nbsp;'.lng('photos').'</h4>';
		}
		if($layout == 'grid') {
			$imagesText .= '<div class="grid-x grid-margin-x">';
		}
		if(isset($password) and $password != $_REQUEST['password']) {
			if($layout != 'grid') {
				$imagesText .= '<div class="grid-x grid-margin-x">';
			}
			//$imagesText .= 'This directory is password protected! Enter the password in the field below to access it.';
			$imagesText .= '<div class="cell small-12 medium-8 large-6 medium-offset-2 large-offset-3">';
			$imagesText .= '<form action="'.$baseUrl.$_REQUEST['url'].'" method="post">';
			$imagesText .= '<fieldset class="fieldset">';
    		$imagesText .= '<legend>'.lng('enter_password_to_unlock').'</legend>';

			$imagesText .= '<div class="input-group">';
			$imagesText .= '<span class="input-group-label"><i class="fi-unlock"></i></span>';

			$imagesText .= '<input class="input-group-field" id="password_input" name="password" type="password">';
			$imagesText .= '<div class="input-group-button">';
			$imagesText .= '<input type="submit" class="button" value="'.lng('submit').'">';
			$imagesText .= '</div>';
			$imagesText .= '</div>';

			$imagesText .= '</fieldset>';
			$imagesText .= '</form>';
			$imagesText .= '<script type="text/javascript">';
			$imagesText .= 'document.getElementById("password_input").focus();';
			$imagesText .= '</script>';
			$imagesText .= '</div>';
			if($layout != 'grid') {
				$imagesText .= '</div>';
			}
		} else {
			asort($files);
			reset($files);
			$first = true;
			foreach($files as $file) {
				// Generate thumbnail and web image if it doesn't exist yet
				if($layout == 'grid') {
					$thumbnailCut = true;
				} else {
					$thumbnailCut = false;
				}
				$thumbnail = createImage($file, $thumbnailMaxWidth, $thumbnailMaxHeight, $thumbnailCut);
				$large = createImage($file, $largeMaxWidth, $largeMaxHeight);
				// Overwrite the default share image
				if($first == true) {
					$first = false;
					$shareImage = substr(createImage($file, 1200, 627, true), 4);
					$shareImage = 'tmp/'.rawurlencode($shareImage);

				}
				if($layout == 'grid') {
					$imagesText .= '<div class="gallery-cell-grid cell '.$imageColumns.' text-center gallery-cell">';
				} else {
					$imagesText .= '<div class="gallery-cell-fluid gallery-cell">';
				}
				$imagesText .= '<a href="'.$large.'" data-lightbox="images" data-title="';
				if(!empty($csv_titles[stripDirectory($file)])) {
					$imagesText .= $csv_titles[stripDirectory($file)];
				} else {
					$imagesText .= stripDirectory($file);
				}
				$imagesText .= '"><img class="thumb" src="'.$thumbnail.'" /></a>';
				$imagesText .= '<div class="gallery-cell-download">';
				$imagesText .= '<a href="'.$file.'" target="_blank" title="'.lng('download_original').'"><i class="fi-download"></i></a>';
				$imagesText .= '</div>';
				$imagesText .= '</div>';
			}
		}
		if($layout == 'grid') {
			$imagesText .= '</div>';
		}
		$imagesText .= '<br />';
		// Bottom text
		if(!isset($password) or (isset($password) and $password == $_REQUEST['password'])) {
			if(!empty($text_bottom)) {
				$imagesText .= $text_bottom;
				$imagesText .= '<br />';
			}
		}

	}
}
$shareImageSize = getimagesize($baseUrl.$shareImage);

?><!doctype html>
	<html class="no-js" lang="hu">
	<head>
		<base href="<?php echo $baseUrl; ?>" />
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php echo $siteName; ?></title>
		<meta itemprop="name" content="<?php echo $shareName; ?>">
		<meta itemprop="description" content="<?php echo $siteDescription; ?>">
		<meta itemprop="image" content="<?php echo $baseUrl.$shareImage; ?>">
		<!-- Facebook open graph -->
		<meta property="og:url" content="<?php echo $baseUrl.$_REQUEST['url']; ?>" />
		<meta property="og:type"   content="website" />
		<meta property="og:title" content="<?php echo $shareName; ?>" />
		<meta property="og:site_name" content="<?php echo $siteName; ?>"/>
		<meta property="og:description" content="<?php echo $siteDescription; ?>" />
		<meta property="og:image" content="<?php echo $baseUrl.$shareImage; ?>" />
		<meta property="og:image:width" content="<?php echo $shareImageSize[0]; ?>" />
		<meta property="og:image:height" content="<?php echo $shareImageSize[1]; ?>" />
		<!-- Stylesheets -->
		<link rel="stylesheet" href="css/loading.css">
		<link rel="stylesheet" href="css/foundation.min.css">
		<link rel="stylesheet" href="css/style-<?php echo $_COOKIE['style']; ?>.css?p=<?php echo filemtime('css/style-'.$_COOKIE['style'].'.css'); ?>">
		<link rel="stylesheet" href="css/foundation-icons.css">
		<link rel="stylesheet" href="css/lightbox.css" />
		<!-- Icons -->
		<link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
		<link rel="manifest" href="img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<?php if($googleAnalyticsId != ''): ?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $googleAnalyticsId;?>"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', '<?php echo $googleAnalyticsId;?>');
		</script>
		<?php endif; ?>
	</head>
	<body>
		<div class="loading" id="loading">Loading&#8230;</div>
		<?php if(isset($permissionWarning)){echo $permissionWarning;}?>
		<div id="header">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="cell small-12">
						<img id="logo" src="img/logo-<?php echo $_COOKIE['style']; ?>.png" />
						<?php echo $siteName; ?>
					</div>
				</div>
			</div>
		</div>
		<div id="breadcrumb">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="cell small-12">
						<?php echo $breadcrumbText; ?>
					</div>
				</div>
			</div>
		</div>
		<div id="main">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
						<?php
							echo '<div class="cell small-12">';
							echo $titleText;
							echo '</div>';
							// Left text
							if(!isset($password) or (isset($password) and $password == $_REQUEST['password'])) {
								if(!empty($text_left)) {
									echo '<div class="cell small-12 medium-4 large-3">';
									echo $text_left;
									echo '<br />';
									echo '</div>';
								}
							}
							if(!isset($password) or (isset($password) and $password == $_REQUEST['password'])) {
								if(empty($text_left) and empty($text_right)) {
									echo '<div class="cell small-12">';
								} elseif(!empty($text_left) and empty($text_right)) {
									echo '<div class="cell small-12 medium-8 large-9">';
								} elseif(empty($text_left) and !empty($text_right)) {
									echo '<div class="cell small-12 medium-8 large-9">';
								} else {
									echo '<div class="cell small-12 medium-4 large-6">';
								}
							} else {
								echo '<div class="cell small-12">';
							}
							echo $directoriesText;
							echo $imagesText;
							echo '</div>';
							// Right text
							if(!isset($password) or (isset($password) and $password == $_REQUEST['password'])) {
								if(!empty($text_right)) {
									echo '<div class="cell small-12 medium-4 large-3">';
									echo $text_right;
									echo '<br />';
									echo '</div>';
								}
							}
						?>
				</div>
			</div>
		</div>
		<div id="footer">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="cell small-12 medium-6 text-center medium-text-left">
						<?php
							if($_COOKIE['style'] == 'light') {
								echo '<a href="'.$baseUrl.$_REQUEST['url'].'" onclick="createCookie(\'style\', \'dark\', 365);">'.lng('switch_to_dark_mode').'</a>';
							} else {
								echo '<a href="'.$baseUrl.$_REQUEST['url'].'" onclick="createCookie(\'style\', \'light\', 365);">'.lng('switch_to_light_mode').'</a>';
							}
						?>
						&nbsp;|&nbsp;
						<a href="javascript://" onclick="share('<?php echo $baseUrl.$_REQUEST['url']; ?>', '<?php echo lng('gallery_url_copied_to_clipboard'); ?>');"><?php echo lng('copy_gallery_url'); ?></a>
					</div>
					<div class="cell small-12 medium-6 text-center medium-text-right">
						EasyPhotobook v<?php echo file_get_contents('version.txt'); ?>&nbsp;|&nbsp;<a href="README.md" target="_blank"><?php echo lng('readme'); ?></a>&nbsp;|&nbsp;<a href="https://bitbucket.org/tenchi39/easyphotobook/src/master/" target="_blank">Bitbucket</a>
					</div>
				</div>
			</div>
		</div>
		<br>
		<script src="js/functions.js"></script>
		<script src="js/jquery.min.js"></script>
		<script src="js/foundation.min.js"></script>
		<script src="js/lightbox.min.js"></script>
		<script>

			lightbox.option({
				'albumLabel': '<?php echo lng('lightbox_images'); ?>', 
				'wrapAround': true, 
				'disableScrolling': true
			})

			$(document).ready(function() {
				$(document).foundation();
			});

			$(window).on("load", function() {
				$( "#loading" ).fadeOut( "slow", function() {
				});			
			});

			<?php if($disableRightClick == true): ?>
			document.addEventListener('contextmenu', event => event.preventDefault());
			<?php endif; ?>

		</script>
	</body>
	</html>